package SenjiCorps;

import SenjiCorps.chiffre.asymetrique.Rsa;
import SenjiCorps.chiffre.simple.Cesar;
import SenjiCorps.chiffre.simple.Decalage;
import SenjiCorps.chiffre.simple.Vigenere;
import SenjiCorps.commons.SubstitUtils;
import SenjiCorps.crypte.TeraEra;
import SenjiCorps.hash.Md5;
import SenjiCorps.hash.Sha1;
import SenjiCorps.hash.Sha256;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchAlgorithmException {

        boolean isHelp = false, isAction = false, isReady = false;
        enumAction action = enumAction.AIDE;
        String message = null, cle = null, methode = null, filepath = null, checksumTest = null;
        int decalage = 0;

        if (args.length != 0) {
            for (int ind = 0; ind < args.length && !isHelp && !isReady; ind++) {
                System.out.println("Argument : " + args[ind]);
                if (args[ind].equals("-h")) {
                    isHelp = true;
                    isReady = true;
                } else if (!isAction && !isHelp) {
                    if (args[ind].equals("-c")) {
                        System.out.println("Cryptage demandé");
                        if (ind != args.length - 1) {
                            ind++;
                            methode = args[ind];
                            System.out.println("-> méthode : " + methode);
                            isAction = true;
                            action = enumAction.CRYPT;
                        }
                    } else if (args[ind].equals("-d")) {
                        System.out.println("Decryptage demandé");
                        if (ind != args.length - 1) {
                            ind++;
                            methode = args[ind];
                            System.out.println("-> méthode : " + methode);
                            isAction = true;
                            action = enumAction.DECRYPT;
                        }
                    } else if (args[ind].equals("-h")) {
                        System.out.println("Hashage demandé");
                        if (ind != args.length - 1) {
                            ind++;
                            methode = args[ind];
                            System.out.println("-> méthode : " + methode);
                            isAction = true;
                            action = enumAction.HASH;
                        }
                    } else if (args[ind].equals("-cs")) {
                        System.out.println("Checksum demandé");
                        if (ind != args.length - 1) {
                            ind++;
                            methode = args[ind];
                            System.out.println("-> méthode : " + methode);
                            isAction = true;
                            action = enumAction.CHECKSUM;
                        }
                    } else if (args[ind].equals("-s")) {
                        System.out.println("Signage demandé");
                        if (ind != args.length - 1) {
                            ind++;
                            methode = args[ind];
                            System.out.println("-> méthode : " + methode);
                            isAction = true;
                            action = enumAction.SIGNE;
                        }
                    }
                } else {
                    switch (action) {
                        case CRYPT:
                            System.out.println("Parsing des arguments de cryptage.");
                            if (args[ind].equals("-m")) {
                                System.out.println("Message reçu");
                                if (ind != args.length - 1) {
                                    ind++;
                                    message = args[ind];
                                    System.out.print("-> ");
                                    System.out.println(message);
                                }
                            } else if (args[ind].equals("-k")) {
                                System.out.println("Clé/Taille de clé reçu");
                                if (ind != args.length - 1) {
                                    ind++;
                                    cle = args[ind];
                                    System.out.print("-> ");
                                    System.out.println(cle);
                                }
                            } else if (args[ind].equals("-x")) {
                                System.out.println("Décalage reçu");
                                if (ind != args.length - 1) {
                                    ind++;
                                    decalage = Integer.parseInt(args[ind]);
                                    System.out.print("-> ");
                                    System.out.println(decalage);
                                }
                            }
                            if (message != null && !message.isEmpty() && (methode.equals("Cesar") || (cle != null && !cle.isEmpty()) || (decalage != 0))) {
                                System.out.println("Le cryptage est prêt à être lancé.");
                                isReady = true;
                            }
                            break;
                        case DECRYPT:
                            if (args[ind].equals("-m")) {
                                if (ind != args.length - 1) {
                                    ind++;
                                    message = args[ind];
                                }
                            } else if (args[ind].equals("-k")) {
                                if (ind != args.length - 1) {
                                    ind++;
                                    cle = args[ind];
                                }
                            } else if (args[ind].equals("-x")) {
                                if (ind != args.length - 1) {
                                    ind++;
                                    decalage = Integer.parseInt(args[ind]);
                                }
                            }
                            if (message != null && !message.isEmpty() && ((cle != null && !cle.isEmpty()) || (decalage != 0))) {
                                isReady = true;
                            }
                            break;
                        case HASH:
                            if (args[ind].equals("-m")) {
                                if (ind != args.length - 1) {
                                    ind++;
                                    message = args[ind];
                                }
                            } else if (args[ind].equals("-f")) {
                                if (ind != args.length - 1) {
                                    ind++;
                                    filepath = args[ind];
                                }
                            }
                            if ((message != null && !message.isEmpty()) || (filepath != null && !filepath.isEmpty())) {
                                isReady = true;
                            }
                            break;
                        case CHECKSUM:
                            if (args[ind].equals("-f")) {
                                if (ind != args.length - 1) {
                                    ind++;
                                    filepath = args[ind];
                                }
                            } else if (args[ind].equals("-t")) {
                                if (ind != args.length - 1) {
                                    ind++;
                                    checksumTest = args[ind];
                                }
                            }
                            if ((filepath != null && !filepath.isEmpty()) && (checksumTest != null && !checksumTest.isEmpty())) {
                                isReady = true;
                            }
                            break;
                        case SIGNE:
                            break;
                    }
                }
            }
        } else {
            isHelp = true;
        }

        if (isReady) {
            switch (action) {
                case CRYPT:
                    String messagePurge = SubstitUtils.purge(message);
                    String resultat = null;
                    if (methode.equals("Cesar")) {
                        resultat = Cesar.crypte(messagePurge);
                    } else if (methode.equals("Vigenere")) {
                        resultat = Vigenere.crypte(messagePurge, cle);
                    } else if (methode.equals("Decalage")) {
                        resultat = Decalage.crypte(messagePurge, decalage);
                    } else if (methode.equals("TeraEra")) {
                        resultat = TeraEra.crypt(message);
                    } else if (methode.equals("RSA")) {
                        int taille = Integer.valueOf(cle);
                        if (taille < 512) {
                            taille = 512;
                        }
                        Rsa.init(taille);
                        try {
                            resultat = Rsa.cryptWithClass(message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.print("Résultat du cryptage : de ");
                    System.out.println(messagePurge);
                    System.out.println(resultat);
                    System.out.println("-----------------------");
                    if (methode.equals("RSA")) {
                        System.out.println("-----------------------");
                        System.out.println(Rsa.string());
                        System.out.println("-----------------------");
                    }
                    break;
                case DECRYPT:
                    String messagePurgeD = SubstitUtils.purge(message);
                    String resultatD = null;
                    if (methode.equals("Cesar")) {
                        resultatD = Cesar.deCrypte(messagePurgeD);
                    } else if (methode.equals("Vigenere")) {
                        resultatD = Vigenere.deCrypte(messagePurgeD, cle);
                    } else if (methode.equals("Decalage")) {
                        resultatD = Decalage.deCrypte(messagePurgeD, decalage);
                    } else if (methode.equals("TeraEra")) {
                        resultatD = TeraEra.deCrypt(message);
                    } else if (methode.equals("RSA")) {
                        // A faire
                    }
                    System.out.print("Résultat du décryptage : de ");
                    System.out.println(messagePurgeD);
                    System.out.println(resultatD);
                    System.out.println("-----------------------");
                    if (methode.equals("RSA")) {
                        System.out.println("-----------------------");
                        System.out.println(Rsa.string());
                        System.out.println("-----------------------");
                    }
                    break;
                case HASH:
                    boolean hashMess = false, hashFile = false;
                    if (message != null && !message.isEmpty()) {
                        hashMess = true;
                    } else {
                        hashFile = true;
                    }
                    String hash = null;
                    if (methode.equals("MD5")) {
                        if (hashFile) {
                            hash = Md5.checksum(filepath);
                        } else {
                            hash = Md5.md5(message);
                        }
                    } else if (methode.equals("SHA-1")) {
                        if (hashFile) {
                            hash = Sha1.checksum(filepath);
                        } else {
                            hash = Sha1.sha1(message);
                        }
                    } else if (methode.equals("SHA-256")) {
                        if (hashFile) {
                            hash = Sha256.checksum(filepath);
                        } else {
                            hash = Sha256.sha256(message);
                        }
                    }
                    System.out.print("Résultat du hash : de ");
                    if (hashFile) {
                        System.out.println(filepath);
                    } else {
                        System.out.println(message);
                    }
                    System.out.println(hash);
                    System.out.println("-----------------------");
                    break;
                case CHECKSUM:
                    boolean check = false;
                    if (methode.equals("MD5")) {
                        check = Md5.verifyChecksum(filepath, checksumTest);
                    } else if (methode.equals("SHA-1")) {
                        check = Sha1.verifyChecksum(filepath, checksumTest);
                    } else if (methode.equals("SHA-256")) {
                        check = Sha256.verifyChecksum(filepath, checksumTest);
                    }
                    System.out.print("Résultat du hash : de ");
                    System.out.println(message);
                    System.out.println(check);
                    System.out.println("-----------------------");
                    break;
                case AIDE:
                default:
                    help();
            }
        } else {
            help();
        }
    }

    private static void help() throws IOException {
        Properties hash = new Properties();
        FileInputStream fis = new FileInputStream(new File("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/hash.properties"));
        hash.load(fis);

        Properties crypt = new Properties();
        fis = new FileInputStream(new File("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/crypt.properties"));
        crypt.load(fis);

        StringBuilder str = new StringBuilder();
        str.append("Voici la liste des algorithmes de hashage disponibles :\n");
        for (Object keys : hash.keySet()) {
            str.append("\t- ");
            str.append(keys);
            str.append("\n");
        }
        str.append("\n");
        str.append("Voici la liste des algorithmes de chiffrage/cryptage disponibles :\n");
        for (Object keys : crypt.keySet()) {
            str.append("\t- ");
            str.append(keys);
            str.append("\n");
        }

        System.out.println("Affichage des actions possibles :");
        System.out.println("---------------------------------");
        System.out.println(str.toString());
        System.out.println("---------------------------------");
        System.out.println("Pour le cryptage :");
        System.out.println("-c <méthode> -m <message> (-k <clé> | -d <décalage>)");
        System.out.println("---------------------------------");
        System.out.println("Pour le décryptage :");
        System.out.println("-d <méthode> -m <message> (-k <clé> | -x <décalage>)");
        System.out.println("---------------------------------");
        System.out.println("Pour le hashage :");
        System.out.println("-h <méthode> (-m <message> | -f <chemin du fichier>)");
        System.out.println("---------------------------------");
        System.out.println("Pour le checksum :");
        System.out.println("-cs <méthode> (-m <message> | -f <chemin du fichier>) -t <checksum a tester>");
        System.out.println("---------------------------------");
    }

    enum enumAction {CRYPT, DECRYPT, HASH, CHECKSUM, SIGNE, AIDE}
}
