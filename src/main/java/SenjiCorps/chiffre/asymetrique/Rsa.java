package SenjiCorps.chiffre.asymetrique;

import SenjiCorps.commons.Pair;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import java.math.BigInteger;
import java.security.*;

/**
 * Modify by vrichard on 06/10/2015.
 */
public class Rsa {

    private final static SecureRandom random = new SecureRandom();

    private static BigInteger p;
    private static BigInteger q;
    private static BigInteger n;
    private static BigInteger phi;
    private static BigInteger e;
    private static BigInteger d;

    private static KeyPair keyPair;

    private static boolean init = false;

    // generate an N-bit (roughly) public and private key
    private Rsa(int N) {

        /*
        Initialisation du RSA à l'ancienne.
         */
        p = BigInteger.probablePrime(N, random);
        q = BigInteger.probablePrime(N, random);
        n = p.multiply(q);
        phi = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
        System.out.println("[Rsa] Calcul de e");
        int nbE = 0;
        do {
            e = new BigInteger(2 * N, random);
            nbE++;
            if (nbE > 10) {
                System.out.println("[Rsa] Calcul de e : x" + nbE);
            }
        } while ((e.compareTo(phi) != 1) || (e.gcd(phi).compareTo(BigInteger.ONE) != 0));

        d = e.modInverse(phi);

        /*
        Initialisation du RSA avec les classes prévues pour.
         */
        KeyPairGenerator keygen = null;
        try {
            keygen = KeyPairGenerator.getInstance("RSA");
            if (N < 512) {
                N = 512;
            }
            keygen.initialize(N);
            keyPair = keygen.generateKeyPair();
        } catch (NoSuchAlgorithmException e1) {
            System.out.println("Exception lors de l'init du RSA avec les classes prévues pour.");
        }
    }

    public static boolean isInit() {
        return init;
    }

    public static void init(int tailleCle) {
        if (!isInit()) {
            new Rsa(tailleCle);
            init = true;
        }
    }

    public static String string() {
        StringBuilder s = new StringBuilder();
        if (isInit()) {
            s.append("[Rsa.string] public  = ");
            s.append(n);
            s.append(" ");
            s.append(e);
            s.append("\n");

            s.append("[Rsa.string] private = ");
            s.append(n);
            s.append(" ");
            s.append(d);
            s.append("\n");

            PublicKey key = keyPair.getPublic();
            s.append("[Rsa.string] public Class  = ");
            s.append(key.toString());
            s.append("\n");

            PrivateKey pkey = keyPair.getPrivate();
            s.append("[Rsa.string] private Class  = ");
            s.append(pkey.toString());
            s.append("\n");
        } else {
            s.append("[Rsa.string] RSA n'est pas initialisé.\n");
        }
        return s.toString();
    }

    public static BigInteger crypt(String clair, int tailleCle) {
        BigInteger message = Pair.strToBigInt(clair);

        return crypt(message, tailleCle);
    }

    public static BigInteger crypt(BigInteger clair, int tailleCle) {
        if (!isInit()) {
            init(tailleCle);
        }
        //System.out.println(string());

        System.out.println("[Rsa.crypt] Chiffrage de " + clair);
        BigInteger encrypt = clair.modPow(e, n);
        System.out.println("[Rsa.crypt] Chiffrage fini.");

        return encrypt;
    }

    public static BigInteger crypt(BigInteger clair, BigInteger e, BigInteger n) {

        System.out.println("[Rsa.crypt] Chiffrage de " + clair);
        BigInteger encrypt = clair.modPow(e, n);
        System.out.println("[Rsa.crypt] Chiffrage fini.");

        return encrypt;
    }

    public static BigInteger deCrypt(String crypt, int tailleCle) {
        BigInteger message = Pair.strToBigInt(crypt);

        return deCrypt(message, tailleCle);
    }

    public static BigInteger deCrypt(BigInteger crypt, int tailleCle) {
        if (!isInit()) {
            init(tailleCle);
        }
        //System.out.println(string());

        System.out.println("[Rsa.crypt] Dechiffrage de " + crypt);
        BigInteger decrypt = crypt.modPow(d, n);
        System.out.println("[Rsa.crypt] Dechiffrage fini.");

        return decrypt;
    }

    public static BigInteger deCrypt(BigInteger crypt, BigInteger d, BigInteger n) {

        System.out.println("[Rsa.crypt] Dechiffrage de " + crypt);
        BigInteger decrypt = crypt.modPow(d, n);
        System.out.println("[Rsa.crypt] Dechiffrage fini.");

        return decrypt;
    }

    public static String cryptWithClass(String plaintext) throws Exception {
        if (!isInit()) {
            init(512);
        }
        PublicKey key = keyPair.getPublic();
        //System.out.println("[RSA.cryptWithClass] Public key : " + key.toString());
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] ciphertext = cipher.doFinal(plaintext.getBytes("UTF8"));
        return encodeBASE64(ciphertext);
    }

    public static String deCryptWithClass(String ciphertext) throws Exception {
        if (!isInit()) {
            init(512);
        }
        PrivateKey key = keyPair.getPrivate();
        //System.out.println("[RSA.cryptWithClass] Private key : " + key.toString());
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

        cipher.init(Cipher.DECRYPT_MODE, key);

        byte[] plaintext = cipher.doFinal(decodeBASE64(ciphertext));
        return new String(plaintext, "UTF8");
    }

    private static String encodeBASE64(byte[] bytes) {
        BASE64Encoder b64 = new BASE64Encoder();
        return b64.encode(bytes);
    }

    private static byte[] decodeBASE64(String text) throws Exception {
        BASE64Decoder b64 = new BASE64Decoder();
        return b64.decodeBuffer(text);
    }

    public String toString() {
        return Rsa.string();
    }
}
