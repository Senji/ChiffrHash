package SenjiCorps.chiffre.simple;

import SenjiCorps.commons.SubstitUtils;

/**
 * Created by vrichard on 06/10/2015.
 */
public class Cesar {

    // Décalage de l'alphabet d'un nombre de fois prédéfini : 3.

    public static String crypte(String message) {
        char[] chars = message.toCharArray();
        StringBuilder res = new StringBuilder();
        for (char car : chars) {
            res.append(SubstitUtils.decalage(car, 3));
        }
        return res.toString();
    }

    public static String deCrypte(String message) {
        char[] chars = message.toCharArray();
        StringBuilder res = new StringBuilder();
        for (char car : chars) {
            res.append(SubstitUtils.decalage(car, -3));
        }
        return res.toString();
    }

}
