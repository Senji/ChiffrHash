package SenjiCorps.chiffre.simple;

import SenjiCorps.commons.SubstitUtils;

/**
 * Created by vrichard on 13/10/2015.
 */
public class Decalage {

    // Décalage de l'alphabet d'un nombre de fois prédéfini.

    public static String crypte(String message, int dec) {
        char[] chars = message.toCharArray();
        StringBuilder res = new StringBuilder();
        for (char car : chars) {
            res.append(SubstitUtils.decalage(car, dec));
        }
        return res.toString();
    }

    public static String deCrypte(String message, int dec) {
        char[] chars = message.toCharArray();
        StringBuilder res = new StringBuilder();
        if (dec < 0) {
            dec *= -1;
        }
        for (char car : chars) {
            res.append(SubstitUtils.decalage(car, -dec));
        }
        return res.toString();
    }
}
