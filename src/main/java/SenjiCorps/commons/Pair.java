package SenjiCorps.commons;

import com.sun.istack.internal.NotNull;

import java.math.BigInteger;

/**
 * Created by vrichard on 06/10/2015.
 */
public class Pair implements Comparable<Pair> {

    private BigInteger cle;
    private BigInteger valeur;

    public Pair(BigInteger cle, BigInteger valeur) {
        this.cle = cle;
        this.valeur = valeur;
    }

    public Pair(String cle, BigInteger valeur) {
        this(strToBigInt(cle), valeur);
    }

    public Pair(BigInteger cle, String valeur) {
        this(cle, strToBigInt(valeur));
    }

    public Pair(String cle, String valeur) {
        this(strToBigInt(cle), strToBigInt(valeur));
    }

    public static BigInteger strToBigInt(String value) {
        return new BigInteger(value.getBytes());
    }

    public static String bigIntToStr(BigInteger value) {
        return new String(value.toByteArray());
    }

    public BigInteger getCle() {
        return cle;
    }

    public void setCle(BigInteger cle) {
        this.cle = cle;
    }

    public void setCle(String cle) {
        this.cle = strToBigInt(cle);
    }

    public String getCleString(int radix) {
        return cle.toString(radix);
    }

    public String getCleString() {
        return getCleString(10);
    }

    public BigInteger getValeur() {
        return valeur;
    }

    public void setValeur(BigInteger valeur) {
        this.valeur = valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = strToBigInt(valeur);
    }

    public String getValeurString(int radix) {
        return valeur.toString(radix);
    }

    public String getValeurString() {
        return getValeurString(10);
    }

    public boolean equals(Object x) {
        // This test is just an optimization, which may or may not help
        if (x == this)
            return true;

        if (!(x instanceof Pair))
            return false;

        Pair xPair = (Pair) x;

        return xPair.getCle().equals(this.cle) && xPair.getValeur().equals(this.valeur);

    }

    @NotNull
    public int compareTo(Pair val) {
        if (this.equals(val)) {
            return 0;
        } else {
            if (val.getCle().equals(this.cle)) {
                return val.getValeur().compareTo(this.valeur);
            } else {
                return val.getCle().compareTo(this.cle);
            }
        }
    }
}
