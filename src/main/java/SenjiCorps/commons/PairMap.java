package SenjiCorps.commons;

import java.math.BigInteger;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Created by vrichard on 06/10/2015.
 */
public class PairMap extends TreeSet<Pair> {

    Dictionary<String, String> dico;

    public BigInteger getValeurCle(BigInteger cle) {
        Iterator<Pair> it = this.iterator();
        BigInteger result = null;
        while (result == null && it.hasNext()) {
            Pair temp = it.next();
            if (temp.getCle().equals(cle)) {
                result = temp.getValeur();
            } else if (temp.getValeur().equals(cle)) {
                result = temp.getCle();
            }
        }
        return result;
    }

    public BigInteger getValeur(BigInteger cle) {
        Iterator<Pair> it = this.iterator();
        BigInteger result = null;
        while (result == null && it.hasNext()) {
            Pair temp = it.next();
            if (temp.getCle().equals(cle)) {
                result = temp.getValeur();
            }
        }
        return result;
    }

    public BigInteger getCle(BigInteger valeur) {
        Iterator<Pair> it = this.iterator();
        BigInteger result = null;
        while (result == null && it.hasNext()) {
            Pair temp = it.next();
            if (temp.getValeur().equals(valeur)) {
                result = temp.getCle();
            }
        }
        return result;
    }

}
