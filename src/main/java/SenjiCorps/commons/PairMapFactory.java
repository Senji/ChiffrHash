package SenjiCorps.commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Created by vrichard on 06/10/2015.
 */
public class PairMapFactory {

    public static PairMap dicoTeraEraLoader() {
        PairMap dico = new PairMap();
        Properties properties = new Properties();
        int nbLue = 0, nbAjout = 0;

        try {
            FileInputStream fis = new FileInputStream(new File("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties"));

            properties.load(fis);

            for (Map.Entry entry : properties.entrySet()) {
                String cle, valeur;
                nbLue++;
                if (entry.getKey() instanceof String) {
                    cle = ((String) entry.getKey()).substring(1, ((String) entry.getKey()).length() - 1);
                } else {
                    System.err.println("[dicoTeraEraLoader] Une clé n'est pas une string");
                    continue;
                }
                if (entry.getValue() instanceof String) {
                    valeur = ((String) entry.getValue()).substring(1, ((String) entry.getValue()).length() - 1);
                } else {
                    System.err.println("[dicoTeraEraLoader] Une clé n'est pas une string");
                    continue;
                }
                //System.out.println("[dicoTeraEraLoader] Ajout de : '" + cle + "':'" + valeur + "'");
                dico.add(new Pair(cle, valeur));
                nbAjout++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("[dicoTeraEraLoader] TeraEraDico.properties non trouvé.");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("[dicoTeraEraLoader] Erreur à la lecture de TeraEraDico.properties.");
        }
        System.out.println("[dicoTeraEraLoader] Nombre d'entrée lue " + nbLue + ", nombre d'entrée ajoutée " + nbAjout + ".");
        return dico;
    }
}
