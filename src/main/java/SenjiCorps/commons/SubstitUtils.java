package SenjiCorps.commons;

import java.math.BigInteger;
import java.text.Normalizer;

/**
 * Created by vrichard on 06/10/2015.
 */
public class SubstitUtils {

    public static BigInteger crypt(String cle, PairMap dictionnaire) {
        return crypt(Pair.strToBigInt(cle), dictionnaire);
    }

    public static BigInteger decrypt(String valeur, PairMap dictionnaire) {
        return decrypt(Pair.strToBigInt(valeur), dictionnaire);
    }

    public static boolean isInDictionnaire(String valeur, PairMap dictionnaire) {
        return isInDictionnaire(Pair.strToBigInt(valeur), dictionnaire);
    }

    public static BigInteger crypt(BigInteger cle, PairMap dictionnaire) {
        return dictionnaire.getValeur(cle);
    }

    public static BigInteger decrypt(BigInteger valeur, PairMap dictionnaire) {
        return dictionnaire.getCle(valeur);
    }

    public static boolean isInDictionnaire(BigInteger valeur, PairMap dictionnaire) {
        return dictionnaire.getValeurCle(valeur) != null;
    }

    public static String purge(String base) {
        return Normalizer.normalize(base, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
    }

    public static char decalage(char car, int dec) {
        if (!Character.isLetter(car)) {
            return car;
        }
        int iCar = ((int) car - ((int) 'A'));
        if ((iCar / 26) > 0) {
            iCar = ((int) car - ((int) 'a'));
            return ((char) (((iCar + dec) % 26) + ((int) 'a')));
        } else {
            return ((char) (((iCar + dec) % 26) + ((int) 'A')));
        }
    }

    public static char decalage(char car, char dec, boolean crypt) {
        if (!Character.isLetter(car)) {
            return car;
        }
        int iDec = ((int) dec - ((int) 'A'));
        if ((iDec / 26) > 0) {
            iDec = ((int) dec - ((int) 'a'));
        }
        if (!crypt) {
            iDec = iDec * -1;
        }

        int iCar = ((int) car - ((int) 'A'));
        if ((iCar / 26) > 0) {
            iCar = ((int) car - ((int) 'a'));
            int tmp = iCar + iDec;
            if (tmp < 0) {
                tmp = 26 + tmp;
            } else {
                tmp = tmp % 26;
            }
            return ((char) (tmp + ((int) 'a')));
        } else {
            int tmp = iCar + iDec;
            if (tmp < 0) {
                tmp = 26 + tmp;
            } else {
                tmp = tmp % 26;
            }
            return ((char) (tmp + ((int) 'A')));
        }
    }

}
