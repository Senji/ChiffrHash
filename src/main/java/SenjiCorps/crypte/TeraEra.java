package SenjiCorps.crypte;

import SenjiCorps.commons.Pair;
import SenjiCorps.commons.PairMap;
import SenjiCorps.commons.PairMapFactory;
import SenjiCorps.commons.SubstitUtils;

import java.math.BigInteger;

/**
 * Created by vrichard on 06/10/2015.
 */
public class TeraEra {

    private static PairMap dictionnaire;
    private static boolean isInit;

    private static void init() {
        if (!isInit) {
            dictionnaire = PairMapFactory.dicoTeraEraLoader();
        }
    }

    public static String crypt(String message) {
        if (!isInit) {
            init();
        }

        char[] chars = message.toCharArray();
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < chars.length; i++) {
            BigInteger tmp = SubstitUtils.crypt(String.valueOf(chars[i]), dictionnaire);
            if (tmp != null) {
                result.append(Pair.bigIntToStr(tmp));
            }
        }

        /*
        System.out.println();
        System.out.println("[TeraEra.Crypt] Résultat du cryptage :");
        System.out.println("--------------------------------------");
        System.out.println("[TeraEra.Crypt] Message : " + message);
        System.out.println("--------------------------------------");
        System.out.println(result);
        System.out.println("--------------------------------------");
        System.out.println();
        */

        return result.toString();
    }

    public static String deCrypt(String message) {
        if (!isInit) {
            init();
        }

        if ((message.length() % 4 == 0) && (message.length() > 0)) {

            int nbDecrypt = message.length() / 4;

            char[] chars = message.toCharArray();
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < nbDecrypt; i++) {
                result.append(Pair.bigIntToStr(SubstitUtils.decrypt(message.substring(i * 4, (i + 1) * 4), dictionnaire)));
            }

            /*
            System.out.println();
            System.out.println("[TeraEra.Crypt] Résultat du cryptage :");
            System.out.println("--------------------------------------");
            System.out.println("[TeraEra.Crypt] Message : " + message);
            System.out.println("--------------------------------------");
            System.out.println(result);
            System.out.println("--------------------------------------");
            System.out.println();
            */
            return result.toString();
        } else {
            /*
            System.out.println();
            System.out.println("[TeraEra.Crypt] Résultat du cryptage :");
            System.out.println("--------------------------------------");
            System.out.println("[TeraEra.Crypt] Message : " + message);
            System.out.println("--------------------------------------");
            System.out.println("[TeraEra.Crypt] Erreur, la taille n'est pas un modulo de 4");
            System.out.println("--------------------------------------");
            System.out.println();
            */
            return null;
        }
    }
}
