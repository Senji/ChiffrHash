package SenjiCorps.hash;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by vrichard on 06/10/2015.
 */
public class Md5 {

    /**
     * Hash text
     *
     * @param input
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String md5(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest mDigest = MessageDigest.getInstance("MD5");
        byte[] result = mDigest.digest(input.getBytes("UTF-8"));
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    /**
     * Create file's MD5 checksum
     *
     * @param Filepath and name of a file that is to be verified
     * @return the expeceted MD5 checksum matches the file's MD5 checksum.
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static String checksum(String file) throws NoSuchAlgorithmException, IOException {
        MessageDigest sha1 = MessageDigest.getInstance("MD5");
        FileInputStream fis = new FileInputStream(file);

        byte[] data = new byte[1024];
        int read = 0;
        while ((read = fis.read(data)) != -1) {
            sha1.update(data, 0, read);
        }
        byte[] hashBytes = sha1.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < hashBytes.length; i++) {
            sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    /**
     * Verifies file's MD5 checksum
     *
     * @param Filepath     and name of a file that is to be verified
     * @param testChecksum the expected checksum
     * @return true if the expeceted MD5 checksum matches the file's MD5 checksum; false otherwise.
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static boolean verifyChecksum(String file, String testChecksum) throws NoSuchAlgorithmException, IOException {
        MessageDigest sha1 = MessageDigest.getInstance("MD5");
        FileInputStream fis = new FileInputStream(file);

        byte[] data = new byte[1024];
        int read = 0;
        while ((read = fis.read(data)) != -1) {
            sha1.update(data, 0, read);
        }
        byte[] hashBytes = sha1.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < hashBytes.length; i++) {
            sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        String fileHash = sb.toString();

        return fileHash.equals(testChecksum);
    }


    /*
    //Note : Toutes les variables sont sur 32 bits

    //Définir r comme suit :
    var entier[64] r, k
    r[ 0..15] := {7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22}
    r[16..31] := {5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20}
    r[32..47] := {4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23}
    r[48..63] := {6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21}

    //MD5 utilise des sinus d'entiers pour ses constantes :
    pour i de 0 à 63 faire
        k[i] := floor(abs(sin(i + 1)) × 2^32)
    fin pour

    //Préparation des variables :
    var entier h0 := 0x67452301
    var entier h1 := 0xEFCDAB89
    var entier h2 := 0x98BADCFE
    var entier h3 := 0x10325476

    //Préparation du message (padding) :
    ajouter le bit "1" au message
    ajouter le bit "0" jusqu'à ce que la taille du message en bits soit égale à 448 (mod 512)
    ajouter la taille du message codée en 64-bit little-endian au message

    //Découpage en blocs de 512 bits :
    pour chaque bloc de 512 bits du message
        subdiviser en 16 mots de 32 bits en little-endian w[i], 0 ≤ i ≤ 15

        //initialiser les valeurs de hachage :
        var entier a := h0
        var entier b := h1
        var entier c := h2
        var entier d := h3

        //Boucle principale :
        pour i de 0 à 63 faire
            si 0 ≤ i ≤ 15 alors
                  f := (b et c) ou ((non b) et d)
                  g := i
            sinon si 16 ≤ i ≤ 31 alors
                  f := (d et b) ou ((non d) et c)
                  g := (5×i + 1) mod 16
            sinon si 32 ≤ i ≤ 47 alors
                  f := b xor c xor d
                  g := (3×i + 5) mod 16
            sinon si 48 ≤ i ≤ 63 alors
                f := c xor (b ou (non d))
                g := (7×i) mod 16
            fin si
            var entier temp := d
            d := c
            c := b
            b := ((a + f + k[i] + w[g]) leftrotate r[i]) + b
            a := temp
        fin pour

        //ajouter le résultat au bloc précédent :
        h0 := h0 + a
        h1 := h1 + b
        h2 := h2 + c
        h3 := h3 + d
    fin pour

    var entier empreinte := h0 concaténer h1 concaténer h2 concaténer h3 //(en little-endian)
     */
}
