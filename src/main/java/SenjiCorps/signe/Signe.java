package SenjiCorps.signe;

import SenjiCorps.chiffre.asymetrique.Rsa;
import SenjiCorps.hash.Sha256;

/**
 * Created by vrichard on 09/10/2015.
 */
public class Signe {

    public static String signe(String message, boolean symetrique, int sizeCle) throws Exception {

        String hash = Sha256.sha256(message);

        Rsa.init(sizeCle);
        String crypt = Rsa.cryptWithClass(hash);

        return crypt;
    }

    public static String signeFile(String file, boolean symetrique, int sizeCle) throws Exception {

        String hash = Sha256.checksum(file);

        Rsa.init(sizeCle);
        String crypt = Rsa.cryptWithClass(hash);

        return crypt;
    }
}
