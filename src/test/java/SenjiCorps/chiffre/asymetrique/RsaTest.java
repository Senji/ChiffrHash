package SenjiCorps.chiffre.asymetrique;

import SenjiCorps.commons.Pair;
import junit.framework.TestCase;

import java.math.BigInteger;

/**
 * Created by vrichard on 08/10/2015.
 */
public class RsaTest extends TestCase {


    public void testCrypt() throws Exception {
        System.out.println("Test cryptage");
        BigInteger result = Rsa.crypt("Test", 512);
        System.out.println(result.toString());
        System.out.println();
    }

    public void testDeCrypt() throws Exception {

        System.out.println("Test cryptage");
        BigInteger test = Rsa.crypt("Test", 512);
        System.out.println(test.toString());
        System.out.println("Test decryptage");
        BigInteger result = Rsa.deCrypt(test, 512);
        System.out.println(result.toString());
        System.out.println(Pair.bigIntToStr(result));
        System.out.println();

    }

    public void testCryptWithClass() throws Exception {
        Rsa.init(512);
        System.out.println("Test cryptage avec classe");
        String result = Rsa.cryptWithClass("Test");
        System.out.println(result);
        System.out.println();
    }

    public void testDeCryptWithClass() throws Exception {
        Rsa.init(512);
        System.out.println("Test cryptage avec classe");
        String result = Rsa.cryptWithClass("Test");
        System.out.println(result);
        System.out.println("Test decryptage avec classe");
        result = Rsa.deCryptWithClass(result);
        System.out.println(result);
        System.out.println();
    }
}