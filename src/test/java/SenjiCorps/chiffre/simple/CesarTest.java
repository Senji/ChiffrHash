package SenjiCorps.chiffre.simple;

import junit.framework.TestCase;

/**
 * Created by vrichard on 09/10/2015.
 */
public class CesarTest extends TestCase {

    public void testCrypte() throws Exception {
        System.out.println("Test cryptage \"Cesar\" à la César");
        System.out.println("Cesar -> " + Cesar.crypte("Cesar"));
    }

    public void testDeCrypte() throws Exception {
        System.out.println("Test décryptage \"Cesar\" à la César");
        String test = Cesar.crypte("Cesar");
        System.out.println(test + " -> " + Cesar.deCrypte(test));
    }
}