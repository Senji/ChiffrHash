package SenjiCorps.chiffre.simple;

import junit.framework.TestCase;

/**
 * Created by vrichard on 09/10/2015.
 */
public class VigenereTest extends TestCase {

    public void testCrypte() throws Exception {
        System.out.println("Test de cryptage avec Vigenère : Test, Cle");
        System.out.println("Test -> " + Vigenere.crypte("Test", "Cle"));
    }

    public void testDeCrypte() throws Exception {
        System.out.println("Test de cryptage avec Vigenère : Test, Cle");
        String test = Vigenere.crypte("Test", "Cle");
        System.out.println(test + " -> " + Vigenere.deCrypte(test, "Cle"));
    }
}