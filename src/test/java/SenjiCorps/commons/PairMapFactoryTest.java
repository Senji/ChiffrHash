package SenjiCorps.commons;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by vrichard on 07/10/2015.
 */
public class PairMapFactoryTest extends TestCase {

    public void testDicoTeraEraLoader() throws Exception {
        PairMap test = null;

        test = PairMapFactory.dicoTeraEraLoader();

        Assert.assertTrue(test.size() != 0);
    }
}