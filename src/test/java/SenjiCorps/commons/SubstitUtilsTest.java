package SenjiCorps.commons;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by vrichard on 08/10/2015.
 */
public class SubstitUtilsTest extends TestCase {

    char[] testMin = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    char[] testMaj = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    public void testDecalage() throws Exception {
        char result;
        for (int dec = 0; dec < 28; dec++) {
            for (int i = 0; i < testMin.length; i++) {
                result = SubstitUtils.decalage(testMin[i], dec);
                Assert.assertEquals(testMin[(i + dec) % 26], result);
            }
        }
        for (int dec = 0; dec < 28; dec++) {
            for (int i = 0; i < testMaj.length; i++) {
                result = SubstitUtils.decalage(testMaj[i], dec);
                Assert.assertEquals(testMaj[(i + dec) % 26], result);
            }
        }
    }

    public void testDecalageChar() throws Exception {
        char result;
        char cle;
        for (int dec = 0; dec < 28; dec++) {
            cle = testMin[dec % 26];
            for (int i = 0; i < testMin.length; i++) {
                result = SubstitUtils.decalage(testMin[i], cle, true);
                Assert.assertEquals(testMin[(i + (dec % 26)) % 26], result);
            }
        }
        for (int dec = 0; dec < 28; dec++) {
            cle = testMaj[dec % 26];
            for (int i = 0; i < testMaj.length; i++) {
                result = SubstitUtils.decalage(testMaj[i], cle, true);
                Assert.assertEquals(testMaj[(i + (dec % 26)) % 26], result);
            }
        }
    }
}