package SenjiCorps.hash;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by vrichard on 08/10/2015.
 */
public class Md5Test extends TestCase {

    public void testMd5() throws Exception {
        System.out.println("Hachage avec MD5");
        System.out.println(Md5.md5("Test"));
    }

    public void testChecksum() throws Exception {
        System.out.println("Checksum avec MD5");
        System.out.println(Md5.checksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties"));
    }

    public void testVerifyChecksum() throws Exception {
        System.out.println("Vérification checksum avec MD5");
        String test = Md5.checksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties");
        Assert.assertTrue(Md5.verifyChecksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties", test));
    }
}