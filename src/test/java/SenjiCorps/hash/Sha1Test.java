package SenjiCorps.hash;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by vrichard on 08/10/2015.
 */
public class Sha1Test extends TestCase {

    public void testSha1() throws Exception {
        System.out.println("Hachage avec SHA-1");
        System.out.println(Sha1.sha1("Test"));
    }

    public void testChecksum() throws Exception {
        System.out.println("Checksum avec SHA-1");
        System.out.println(Sha1.checksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties"));
    }

    public void testVerifyChecksum() throws Exception {
        System.out.println("Vérification Checksum avec SHA-1");
        String test = Sha1.checksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties");
        Assert.assertTrue(Sha1.verifyChecksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties", test));
    }
}