package SenjiCorps.hash;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by vrichard on 08/10/2015.
 */
public class Sha256Test extends TestCase {

    public void testSha256() throws Exception {
        System.out.println("Hachage avec SHA-256");
        System.out.println(Sha256.sha256("Test"));
    }

    public void testChecksum() throws Exception {
        System.out.println("Checksum avec SHA-256");
        System.out.println(Sha256.checksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties"));
    }

    public void testVerifyChecksum() throws Exception {
        System.out.println("Vérification du Checksum avec SHA-256");
        String test = Sha256.checksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties");
        Assert.assertTrue(Sha256.verifyChecksum("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties", test));
    }
}