package SenjiCorps.signe;

import junit.framework.TestCase;

/**
 * Created by vrichard on 09/10/2015.
 */
public class SigneTest extends TestCase {

    public void testSigne() throws Exception {
        System.out.println("Test signature d'un message : Test");
        System.out.println("Test -> " + Signe.signe("Test", false, 1024));
    }

    public void testSigneFile() throws Exception {
        System.out.println("Test signature d'un fichier : Test");
        System.out.println("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties -> " + Signe.signe("D:/Users/vrichard/workspace/ChiffrHash/src/main/resources/TeraEraDico.properties", false, 1024));
    }
}